# eMask 捷徑

## 解析 eMask 簡訊
將簡訊解析成字典檔格式，並提供有用資訊，可串接其他服務 (ex: 新增待辦事項，提醒領取口罩)

- [下載 v1.0.1](https://www.icloud.com/shortcuts/5460ec8d9f3b49be97c81790fdff25c8)
- [下載 v1.0.0](https://www.icloud.com/shortcuts/9130680ac05b4e67bc8c0804e00ef646)

輸入：字串
```
您已完成eMask口罩付款，請於1/1~1/13持領取序號1234567890至原指定超商門市領取。
```

輸出：字典檔
```json
{
    "startAt": "2020/1/1",
    "endAt": "2020/1/13",
    "code": "1234567890",
}
```
