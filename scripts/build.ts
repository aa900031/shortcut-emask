import ora from 'ora'
import path from 'path'
import execa from 'execa'
import glob from 'fast-glob'
import mkdirp from 'mkdirp'
import rimraf from 'rimraf'

const PATH_SRC_DIR = path.resolve(__dirname, '../src')
const PATH_DIST_DIR = path.resolve(__dirname, '../dist')

const getDistFilePath = (inputPath: string): string => {
  return inputPath
    .replace(PATH_SRC_DIR, PATH_DIST_DIR)
    .replace(/\.scpl$/, '.shortcut')
}

const createDistDir = async () => {
  await new Promise<void>((resolve, reject) => rimraf(PATH_DIST_DIR, (err) => err ? reject(err) : resolve()))
  await mkdirp(PATH_DIST_DIR)
}

const getSrcFilePaths = async () => {
  return glob(`${PATH_SRC_DIR}/*.scpl`)
}

const execClearDist = async () => {
  const spinner = ora('清空 dist')
  try {
    spinner.start()
    await createDistDir()
    spinner.succeed()
  } catch (error) {
    spinner.fail(error)
  }
}

const execScpl = async (inputFilePath: string, outputFilePath: string) => {
  const spinner = ora(`編譯 ${path.basename(inputFilePath)} -> ${path.basename(outputFilePath)}`)
  try {
    spinner.start()
    await execa('scpl', [inputFilePath, '-o', outputFilePath]);
    spinner.succeed()
  } catch (error) {
    spinner.fail(error)
  }
}

const execBuildSrc = async () => {
  const srcFiles = await getSrcFilePaths()
  for (let index = 0; index < srcFiles.length; index++) {
    const srcFilePath = srcFiles[index]
    const distFilePath = getDistFilePath(srcFilePath)

    await execScpl(srcFilePath, distFilePath)
  }
}

;(async () => {
  await execClearDist()
  await execBuildSrc()
})()
